// SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

using Gtk 4.0;
using Adw 1;

template $TextPiecesSearchEntry : Widget {
    width-request: 225;
    layout-manager: BoxLayout {};
    accessible-role: text_box;

    Image {
        icon-name: "edit-find-symbolic";
    }

    Text text {
        hexpand: true;
        vexpand: true;
        width-chars: 12;
        max-width-chars: 12;

        placeholder-text: C_("search entry", "Find");

        notify::text => $on_text_notify() swapped;
        activate => $on_text_activate() swapped;
    }

    Label info {
        xalign: 1;
        opacity: 0.5;
    }
}