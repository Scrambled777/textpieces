// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#[macro_export]
macro_rules! spawn_async {
    ($future:expr) => {
        ::gtk::glib::MainContext::default().spawn($future)
    };
    (local $future:expr) => {
        ::gtk::glib::MainContext::default().spawn_local($future)
    };
}
