// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gsettings_macro::gen_settings;
use gtk::{gio, glib};
use textpieces_core::ParameterValue;

use crate::config::APP_ID;

/// Hack used to make `rustc` and `rust-analyzer` track file changes.
const _: &str = include_str!("../../data/io.gitlab.liferooter.TextPieces.gschema.xml.in");

// [`gio::Settings`] wrapper derived from GSchema file.
#[gen_settings(file = "./data/io.gitlab.liferooter.TextPieces.gschema.xml.in")]
#[gen_settings_define(
    key_name = "last-params",
    arg_type = "&[ParameterValue]",
    ret_type = "Vec<ParameterValue>"
)]
pub struct Settings;

impl From<ColorScheme> for adw::ColorScheme {
    fn from(value: ColorScheme) -> Self {
        match value {
            ColorScheme::Follow => Self::Default,
            ColorScheme::Dark => Self::ForceDark,
            ColorScheme::Light => Self::ForceLight,
        }
    }
}

impl Default for Settings {
    fn default() -> Self {
        Self::new(APP_ID)
    }
}

impl From<gio::Settings> for Settings {
    fn from(value: gio::Settings) -> Self {
        Self(value)
    }
}
