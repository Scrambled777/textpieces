// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::{gio, prelude::*};

use std::path::Path;

/// Opens file in associated application.
pub async fn open_file(
    path: impl AsRef<Path>,
    window: &impl IsA<gtk::Window>,
) -> anyhow::Result<()> {
    gtk::FileLauncher::new(Some(&gio::File::for_path(path)))
        .launch_future(Some(window))
        .await?;
    Ok(())
}
